package com.example.maheshprasad.enableupnavigationforyourandroidapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView mtext;
    Button goFinal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        goFinal=findViewById(R.id.gotofinal);

        //from intebt we can get data from any activity
        Intent intent = getIntent();

        mtext = findViewById(R.id.message_of_text);
        String messagetext = intent.getStringExtra("message");
        mtext.setText(messagetext);

        goFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SecondActivity.this,FinalActivity.class));
            }
        });
    }
}
